import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";
import Navbar from "./Components/Navbar";
import dummyText from "./DummyText";

import {Modal} from 'reactstrap';

import { Link, animateScroll as scroll } from "react-scroll";

import Home from "./Components/Home/Home";
import About from "./Components/About/About";
import Projects from "./Components/Projects/Projects";
import Services from "./Components/Services/Services";
import Solutions from "./Components/Solutions/Solutions";
import Careers from "./Components/Careers/Careers";
import Login from "./Components/Login/Login";
import Contact from "./Components/Contact/Contact";
class App extends Component {
  render() {
    return (
      <>
      <div className="App">
        <Navbar/>
        <Home />
        {/* <About /> */}
        {/* <Projects /> */}
        <Services/>
        <Solutions />
        <Careers/>
        <Login/>
        <Contact/>
      </div>
      </>
    );
  }
}

export default App;