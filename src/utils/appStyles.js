exports.appStyles = {

  colors: {
    PRIMARY_BG: 'rgba(0, 0, 0, 0.9)'
  },

  container: {
    overflow: 'hidden',
    background: 'rgba(0, 0, 0, 0.9)',
    width: '100%',
    height: '100vh',
  },

  content: {
    color: '#f1f1f1',
    backgroundColor: 'rgba(0, 0, 0, 0.9)',
    width: '100%',
    height: '100%',
    display: 'flex',
    // justifyContent: 'center',
    // alignItems: 'center',
    padding: 0,
  },
}