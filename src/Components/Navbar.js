import React, { Component } from "react";
// import logo from "../../images/ModulusPI.png";
import Button from '@material-ui/core/Button';
import { Link, animateScroll as scroll } from "react-scroll";

export default class Navbar extends Component {
  constructor(props){
    super(props);
    this.state={
        isChanged:false
    }
  }
  toggleNav = ()=>{
    this.setState({isChanged:!this.state.isChanged})
  }
  scrollToTop = () => {
    scroll.scrollToTop();
  };

  render() {
    return (
      <>
      <nav className="nav" id="navbar">
        <div className="nav-header">
        <Link to='home' 
              spy={true}
              smooth={true}
              offset={1}
              duration={500}
              style={{ 
                cursor:'pointer',
                textDecoration: 'none',
                display: 'flex',
                flex: 1,
                fontSize: 40,
                fontWeight: '500',
                color: 'whitesmoke'                
              }}>
            ModulusPI
          </Link>
          </div>
          <div>
          <Button className="nav-list" style={{color:"white",fontSize:30,paddingRight:'2%',cursor:"pointer"}} onClick={this.toggleNav}>
            {this.state.isChanged ? `x` :  String.fromCharCode(9776)}
          </Button>
          </div>
      <div className="PopUp-options" style={{display:this.state.isChanged ?  "grid":"none"}}>     
        <div className="card">
          <ul className="nav-items">
            <li className="nav-item">
              <Link
                activeClass="active"
                to="about"
                spy={true}
                smooth={true}
                offset={1}
                duration={500}
                onClick={this.toggleNav}
              >
                About us
              </Link>
            </li>
            {/* <li className="nav-item">
              <Link
                activeClass="active"
                to="projects"
                spy={true}
                smooth={true}
                offset={0}
                duration={500}
                onClick={this.toggleNav}
              >
                Projects
              </Link>
            </li> */}
            <li className="nav-item">
              <Link
                activeClass="active"
                to="services"
                spy={true}
                smooth={true}
                offset={1}
                duration={500}
                onClick={this.toggleNav}
              >
                Services
              </Link>
            </li>
            <li className="nav-item">
              <Link
                activeClass="active"
                to="solutions"
                spy={true}
                smooth={true}
                offset={1}
                duration={500}
                onClick={this.toggleNav}
              >
                Solutions
              </Link>
            </li>
            <li className="nav-item">
              <Link
                activeClass="active"
                to="careers"
                spy={true}
                smooth={true}
                offset={1}
                duration={500}
                onClick={this.toggleNav}
              >
                Careers
              </Link>
            </li>
            <li className="nav-item">
              <Link
                activeClass="active"
                to="login"
                spy={true}
                smooth={true}
                offset={1}
                duration={500}
                onClick={this.toggleNav}
              >
                Login
              </Link>
            </li>
            <li className="nav-item">
              <Link
                activeClass="active"
                to="contact"
                spy={true}
                smooth={true}
                offset={1}
                duration={500}
                onClick={this.toggleNav}
              >
                Contact
              </Link>
            </li>
          </ul>
          </div>
      </div>
      </nav>
      </>
    );
  }
}
