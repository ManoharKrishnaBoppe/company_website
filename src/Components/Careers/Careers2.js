import React from 'react';
import Search from '@material-ui/icons/Search';
import Person from '@material-ui/icons/Person';

import careers from './careers.css';

const data = [
  {
    position: 'Sr. Software Engineer',
    positionCount: 2,
    technology: 'ORACLE DBA',
    experience: '3-7 Yrs',
    location: 'Hyderabad',
  },
  {
    position: 'Sr. Software Engineer',
    positionCount: 2,
    technology: 'Informatica developer',
    experience: '3-7 Yrs',
    location: 'Hyderabad',
  },
  {
    position: 'Software Engineer',
    positionCount: 2,
    technology: 'ASP.Net MVC & MSSQL developer',
    experience: '3-4 Yrs',
    location: 'Hyderabad',
  },
  {
    position: 'Software Engineer',
    positionCount: 2,
    technology: 'Unity3D & Unreal',
    experience: '1-3 Yrs',
    location: 'Hyderabad',
  },
]

class Careers extends React.Component {
  render() {
    return (
      <div style={styles.body}>
        <div style={{ height: 200 }}></div>
        <div
          style={styles.inputContainer}
          className="inputContainer">
          <Search style={styles.icon} />
          <input type="text" name="search" id="inputID"
            style={styles.textInput}
            placeholder="Search"
          // value={this.state.name}
          // onChange={this.handleChange}
          />
        </div>
        <div style={{ height: 100 }}></div>

        <div style={{ display: 'flex', flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'space-around', alignItems: 'center' }}>
          {
            data.map((item) => (
              <div style={{ display: 'flex', padding: 10 }} className='border'>
                <div style={{ height: 120, width: 580, display: 'flex', }}>
                  <div style={{ flex: 1, display: 'flex', flexDirection: 'column' }}>
                    <div style={{ flex: 1, display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                      <Person style={{ fontSize: 60, color: 'white' }} />
                    </div>
                    <div style={{ flex: 1, display: 'flex', justifyContent: 'center', alignItems: 'center', padding: 4 }}>
                      <h4 style={{ color: 'white' }}>{item.position}</h4>
                    </div>
                  </div>
                  <div style={{ flex: 1, display: 'flex', flexDirection: 'column', }}>
                    <div style={{ flex: 1, display: 'flex', alignItems: 'center', marginLeft: 50 }}>
                      <h5 style={{ color: 'white' }}>{item.technology}</h5>
                    </div>
                    <div style={{ flex: 1, display: 'flex', alignItems: 'center', marginLeft: 50 }}>
                      <h5 style={{ color: 'white' }}>{item.experience}</h5>
                    </div>

                    <div style={{ flex: 1, display: 'flex', alignItems: 'center', marginLeft: 50 }}>
                      <h5 style={{ color: 'white' }}>{item.location}</h5>
                    </div>
                  </div>
                </div>
              </div>
            ))
          }
        </div>
      </div >
    );
  }
}

export default Careers;

const styles = {
  body: {
    flex: 1,
    display: 'flex',
    height: '100vh',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.9)',
  },

  inputContainer: {
    // flex: 1,
    // display: 'flex',
    width: '50%',
    // height: 60,
    // justifyContent: 'center',
    // alignItems: 'center'
  },
  icon: {
    padding: 10,
    color: 'white',
    width: 50,
    height: '100%',
    fontSize: 18,
    backgroundColor: 'rgba(0, 0, 0, 0.9)',
  },
  textInput: {
    outline: 'none',
    border: 'none',
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0, 0, 0, 0.9)',
    padding: 10,
    color: 'grey',
    fontSize: 22,
  }
}
