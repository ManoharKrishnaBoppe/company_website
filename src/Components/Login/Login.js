import React from 'react';
import TextField from '@material-ui/core/TextField';
import { Button } from '@material-ui/core';
import { Link } from "react-scroll";
import GoogleButton from './GoogleButton';
// import Nav from '../nav/Nav';

import bg from '../../assets/bg.png';
class Login extends React.Component {

  constructor() {
    super();
    this.state = {
      username: '',
      password: '',
    }
    this.handleChange = this.handleChange.bind(this);
    this.onClickSignIn = this.onClickSignIn.bind(this);
  }


  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  onClickSignIn(event) {
    event.preventDefault()
    alert(this.state.username + '   ' + this.state.password)
  }

  render() {
    return (
      <>
      {/* <Nav /> */}
      <div style={styles.body} id="login"> 
        <div style={{ flex: 1, display: 'flex', width: '100%' }}>
        </div>
        <div style={{ flex: 1, display: 'flex', width: '100%', flexDirection: 'column', }}>
          <div style={{ flex: 1, display: 'flex', justifyContent: 'center', alignItems: 'center', }}>
            <TextField
              id="filled-secondary"
              label="Username"
              name="username"
              variant="filled"
              color="primary"
              style={styles.textField}
              value={this.state.username}
              onChange={this.handleChange}
            />
          </div>
          <div style={{ flex: 1, display: 'flex', justifyContent: 'center', alignItems: 'center', }}>
            <TextField
              id="filled-secondary"
              label="Password"
              name="password"
              variant="filled"
              color="primary"
              style={styles.textField}
              type="password"
              value={this.state.password}
              onChange={this.handleChange}
            />
          </div>
          <div style={{ flex: 1, display: 'flex', justifyContent: 'center', alignItems: 'center', }}>
            {/* <Button
              variant="contained"
              // onClick={this.onClickSignIn}
              style={{
                outline: 'none', backgroundColor: 'rgba(255, 255, 255, 0.9)', width: '20%', height: '60%', fontWeight: 'bold', fontSize: 15
              }}
              onClick={this.onClickSignIn}
            >
              SIGN IN
          </Button> */}
            <GoogleButton />
          </div>
        </div>
        <div style={{ flex: 1, display: 'flex', width: '100%', justifyContent: 'center', alignItems: 'center', }}>
        <Link to='home' 
              spy={true}
              smooth={true}
              offset={0}
              duration={500}
              style={{ cursor:'pointer',textDecoration: 'none', color: 'white' }}>
            <img src={require('../../assets/ModulusPI.png')} alt='logo' />
          </Link>
        </div>
      </div>
      </>
    );
  }
}

export default Login;

const styles = {
  body: {
    flex: 1,
    display: 'flex',
    height: '100vh',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundImage: `url(${bg})`,
  },
  textField: {
    backgroundColor: 'rgba(255, 255, 255, 0.9)', width: '30%'
  }

}