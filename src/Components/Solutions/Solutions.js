import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
// import Nav from '../../nav/Nav';
import bg from '../../assets/bg.png';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
}));

const data = [
  {
    id: "panel1a-header",
    solution: 'Database Administration',
    description: "We manage your databases and make it available as needed and make it next Gen ready"
  }, {
    id: "panel2a-header",
    solution: 'Server Administration',
    description: "We optimize performance and condition of multiple servers throttling up your business organization"
  }, {
    id: "panel3a-header",
    solution: 'Application Management',
    description: "We are specialized in Monitoring, Troubleshooting, Enhancement, Optimization of your Application"
  }, {
    id: "panel4a-header",
    solution: 'Virtual Reality & Augmented Reality',
    description: "We provide best in class Virtual Reality & Augmented Reality Products and Services"
  }, {
    id: "panel5a-header",
    solution: 'Data Analytics',
    description: "With our top Data Analyst, we provide services of inspecting, cleansing, transforming and modeling data"
  }, {
    id: "panel6a-header",
    solution: 'Data Governance',
    description: "We manage the availability, usability, integrity and security of your data"
  },
];

const Solutions = (props) => {
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);

  const handleChange = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  return (
    <>
   <div style={styles.body} id="solutions">
      
      <div style={{ height: '100%', width: '50%', display: 'flex', alignItems: 'center' }}>
        <div className={classes.root}>
          {
            data.map((item) => (
              <ExpansionPanel expanded={expanded === item.id} onChange={handleChange(item.id)}
                style={{ background: 'rgba(255,255,255, 0.9)' }}>
                <ExpansionPanelSummary
                  expandIcon={<ExpandMoreIcon />}
                  aria-controls="panel1a-content"
                  id={item.id}
                >
                  <Typography className={classes.heading}>{item.solution}</Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                  <Typography>{item.description}</Typography>
                </ExpansionPanelDetails>
              </ExpansionPanel>
            ))
          }

        </div>
      </div>
    </div>
    </>
  );
}

export default Solutions;

const styles = {
  body: {
    flex: 1,
    display: 'flex',
    height: '100vh',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundImage: `url(${bg})`,
  },

}