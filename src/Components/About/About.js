import React from 'react';
import { Button, } from '@material-ui/core';
import { Container, Row, Col } from 'react-bootstrap';
import VRCarousel from './VRCarousel';
import { appStyles } from '../../utils/appStyles'
// import Nav from '../../nav/Nav';
import ITCarousel from './ITCarousel';
import zIndex from '@material-ui/core/styles/zIndex';
// import app from '../../App.css'

class About extends React.Component {

  constructor(props) {
    super(props)
    this.childVR = React.createRef();
    this.childIT = React.createRef();
  }

  onClickVRPrevious = () => {
    this.childVR.current.previous();
  }
  onClickVRNext = () => {
    this.childVR.current.next();
  }

  onClickITPrevious = () => {
    this.childIT.current.previous();
  }
  onClickITNext = () => {
    this.childIT.current.next();
  }

  render() {
    return (
      
      <>
      
      {/* <Nav style={{ zIndex:999}}/> */}
        
        {/* <div style={{ height: 100 }}></div> */}
        
        <div className="" id="about">
            <Row zIndex={0}  style={{ zIndex:0,overflowX:'hidden'}}>
           
                <Col xl style={{zIndex:0, height: '100vh', width: '100%',padding:0 }}>
                
                <div style={{ height: '100vh', width: '100%', }}>
                      <VRCarousel ref={this.childVR} />
                    
                  
                  <div style={styles.content}>
                    <div style={{marginTop:'50vh', flex: 1, display: 'flex', justifyContent: 'space-between', alignItems: 'center'}}>
                      <Button style={{
                        backgroundColor: "rgba('black',0.8)", height: 100, width: 100,
                        borderRadius: '50%', outline: 'none'
                      }} onClick={this.onClickVRPrevious} >
                        <span class="carousel-control-prev-icon"></span>
                      </Button>
                      <Button style={{
                        backgroundColor: "rgba('black',0.8)", height: 100, width: 100,
                        borderRadius: '50%', outline: 'none'
                      }} onClick={this.onClickVRNext} >
                        <span class="carousel-control-next-icon"></span>
                      </Button>
                    </div>
                  </div>
                  </div>
                </Col>
                <Col xl style={{zIndex:0, height: '100vh', width: '100%',padding:0  }}>
                  
                    <div style={{ height: '100vh', width: '100%', }}>
                      <ITCarousel ref={this.childIT} />
                    </div>
                  
                  <div style={styles.content}>
                    <div style={{marginTop:'50vh', flex: 1, display: 'flex', justifyContent: 'space-between', alignItems: 'center', }}>
                      <Button style={{
                        backgroundColor: "rgba('black',0.8)", height: 100, width: 100,
                        borderRadius: '50%', outline: 'none'
                      }} onClick={this.onClickITPrevious} >
                        <span class="carousel-control-prev-icon"></span>
                      </Button>
                      <Button style={{
                        backgroundColor: "rgba('black',0.8)", height: 100, width: 100,
                        borderRadius: '50%', outline: 'none'
                      }} onClick={this.onClickITNext} >
                        <span class="carousel-control-next-icon"></span>
                      </Button>
                    </div>
                  </div>
                </Col>
            </Row>
            </div>
          
           
        
        
        
          
            

            
          
      </>
    )
  }
}

export default About;

const styles = {
  container: {
    width: '100%',
    height: '100vh',
    backgroundColor: appStyles.colors.PRIMARY_BG,
    padding:0,
  },
  content: {
    position: 'absolute',
    background: 'rgba(0, 0, 0,0.2)',
    // borderRadius: 5,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
  },

}
