import React from 'react';
import {
  Carousel,
  CarouselItem,
  CarouselCaption,
} from 'reactstrap';

import VRI1 from '../../assets/VRI1.jpg';
import VRI2 from '../../assets/VRI2.png';
import VRI3 from '../../assets/VRI3.png';
import VRI4 from '../../assets/VRI4.png';
import VRI5 from '../../assets/VRI5.png';
import VRI6 from '../../assets/VRI6.png';

import ARI1 from '../../assets/ARI1.jpg';
import ARI2 from '../../assets/ARI2.jpg';


const items = [
  {
    src: VRI1,
    altText: 'Slide 1',
    captionHeader: 'Virtual Reality',
    captionText: "We provide best in class Virtual Reality & Augmented Reality Products and Services"
  },
  {
    src: VRI2,
    altText: 'Slide 2',
    captionHeader: 'Virtual Reality',
    captionText: 'Providing wide range of Virtual Reality & Augmented Reality Products and Services'
  },
  {
    // src: "https://images.unsplash.com/photo-1493497029755-f49c8e9a8bbe?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",
    src: VRI3,
    altText: 'Slide 3',
    captionHeader: 'Virtual Reality',
    captionText: 'Explore new range of Virtual Reality Products with our Company'
  },
  {
    src: VRI4,
    altText: 'Slide 3',
    captionHeader: 'Virtual Reality',
    captionText: 'Providing wide range of Virtual Reality & Augmented Reality Products and Services'
  },
  {
    src: VRI5,
    altText: 'Slide 3',
    captionHeader: 'Virtual Reality',
    captionText: 'We provide best in class Virtual Reality & Augmented Reality Products and Services'
  },
  {
    src: VRI6,
    altText: 'Slide 3',
    captionHeader: 'Virtual Reality',
    captionText: 'We provide best in class Virtual Reality & Augmented Reality Products and Services'
  },
  {
    src: ARI1,
    altText: 'Slide 3',
    captionHeader: 'Augmented Reality',
    captionText: 'Dive deep into interactive experience of a real-world environment through our solutions'
  },
  {
    src: ARI2,
    altText: 'Slide 3',
    captionHeader: 'Augmented Reality',
    captionText: 'Design and plan portably on the fly'
  }
];

class VRCarousel extends React.Component {

  constructor() {
    super()
    this.state = {
      activeIndex: 0,
      animating: false
    }
  }

  next = () => {
    if (this.state.animating) return;
    const nextIndex = this.state.activeIndex === items.length - 1 ? 0 : this.state.activeIndex + 1;
    this.setState({ activeIndex: nextIndex })
  }

  previous = () => {
    if (this.state.animating) return;
    const nextIndex = this.state.activeIndex === 0 ? items.length - 1 : this.state.activeIndex - 1;
    this.setState({ activeIndex: nextIndex })
  }

  goToIndex = (newIndex) => {
    if (this.state.animating) return;
    this.setState({ activeIndex: newIndex })
  }

  slides = items.map((item) => {
    return (
      <CarouselItem
        onExiting={() => this.setState({ animating: true })}
        onExited={() => this.setState({ animating: false })}
        key={item.src}
      // style={{ justifyContent: 'center', alignItem: 'center' }}
      >
        <img src={item.src} alt={item.altText} style={{ height: '100vh', width: '100%' }} />
        <CarouselCaption className="d-block" captionText={item.captionText} captionHeader={item.captionHeader} />
        {/* <Button>TESTING</Button> */}
      </CarouselItem>
    );
  });

  render() {
    // const { activeIndex, } = this.state;
    return (
      <Carousel
        activeIndex={this.state.activeIndex}
        
        next={this.next}
        previous={this.previous}
      >
        {/* <CarouselIndicators items={items} activeIndex={activeIndex} onClickHandler={this.goToIndex} /> */}
        {this.slides}
        {/* <CarouselControl direction="prev" directionText="Previous" onClickHandler={this.previous} />
        <CarouselControl direction="next" directionText="Next" onClickHandler={this.next}
          className="test"
        /> */}
      </Carousel>
    );
  }

}

export default VRCarousel;
