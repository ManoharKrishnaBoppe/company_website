import React from 'react';
import {
  Carousel,
  CarouselItem,
  CarouselCaption,
} from 'reactstrap';
// import App from '../../App.css'

import Mobile1 from '../../assets/Mobile11.jpg';
import Mobile2 from '../../assets/Mobile2.png';

import ML1 from '../../assets/ML1.png';
import ML2 from '../../assets/ML2.png';

import AI1 from '../../assets/AI1.png';
import AI2 from '../../assets/AI2.png';
import AI3 from '../../assets/AI3.png';

const items = [
  {
    src: Mobile1,
    altText: 'Slide 1',
    captionHeader: 'Application Management',
    captionText: 'We are specialized in Monitoring, Troubleshooting, Enhancement, Optimization of Applications'
  },
  {
    src: Mobile2,
    altText: 'Slide 2',
    captionHeader: 'Database Administration',
    captionText: 'We manage databases and make it available as needed, and make it Secure, reliable and next Gen ready '
  },
  {
    src: "https://images.pexels.com/photos/1148820/pexels-photo-1148820.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
    altText: 'Slide 3',
    captionHeader: 'Data Governance',
    captionText: 'We manage the availability, usability, integrity and security of data'
  },
  {
    src: ML1,
    altText: 'Slide 2',
    captionHeader: 'Machine Learning',
    captionText: 'We are specialized in Monitoring, Troubleshooting, Enhancement, Optimization of your solutions with Machine Learning algorithms'
  },
  {
    src: ML2,
    altText: 'Slide 2',
    captionHeader: 'Machine Learning',
    captionText: 'Gone are the olden days with manual hasstle, in this new era our Machine Learning models are a step ahead building the future'
  },

  {
    src: AI1,
    altText: 'Slide 2',
    captionHeader: 'Artificial Intelligence',
    captionText: 'We offer solutions for your Artificial Intelligence needs'
  },
  {
    src: AI2,
    altText: 'Slide 2',
    captionHeader: 'Artificial Intelligence',
    captionText: 'Our deep learning solutions gives best results'
  },
  {
    src: AI3,
    altText: 'Slide 2',
    captionHeader: 'Artificial Intelligence',
    captionText: 'We offer solutions for your Artificial Intelligence needs'
  },

];

class ITCarousel extends React.Component {
  constructor() {
    super()
    this.state = {
      activeIndex: 0,
      animating: false
    }
  }

  next = () => {
    if (this.state.animating) return;
    const nextIndex = this.state.activeIndex === items.length - 1 ? 0 : this.state.activeIndex + 1;
    this.setState({ activeIndex: nextIndex })
  }

  previous = () => {
    if (this.state.animating) return;
    const nextIndex = this.state.activeIndex === 0 ? items.length - 1 : this.state.activeIndex - 1;
    this.setState({ activeIndex: nextIndex })
  }

  goToIndex = (newIndex) => {
    if (this.state.animating) return;
    this.setState({ activeIndex: newIndex })
  }

  slides = items.map((item) => {
    return (
      <CarouselItem
        onExiting={() => this.setState({ animating: true })}
        onExited={() => this.setState({ animating: false })}
        key={item.src}
      // style={{ justifyContent: 'center', alignItem: 'center' }}
      >
        <img src={item.src} alt={item.altText} style={{ height: '100vh', width: '100%' }} />
        <CarouselCaption className="d-block"  captionText={item.captionText} captionHeader={item.captionHeader} />
        {/* <Button>TESTING</Button> */}
      </CarouselItem>
    );
  });

  render() {
    const { activeIndex, } = this.state;
    return (
      <Carousel
        activeIndex={activeIndex}
        next={this.next}
        previous={this.previous}
        
      >
        {/* <CarouselIndicators items={items} activeIndex={activeIndex} onClickHandler={goToIndex} /> */}
        {this.slides}
        {/* <CarouselControl direction="prev" directionText="Previous" onClickHandler={previous} />
      <CarouselControl direction="next" directionText="Next" onClickHandler={next} /> */}
      </Carousel>
    );
  }
}

export default ITCarousel;
