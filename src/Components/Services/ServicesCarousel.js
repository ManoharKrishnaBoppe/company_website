import React, { useState } from 'react';
import {
  Carousel,
  CarouselItem,
} from 'reactstrap';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardMedia from '@material-ui/core/CardMedia';

import CC1 from '../../assets/CC1.png'

import Mobile2 from '../../assets/Mobile2.png';


import AI1 from '../../assets/AI1.png';
import AI2 from '../../assets/AI2.png';
import AI3 from '../../assets/AI3.png';

import ML1 from '../../assets/ML1.png';
import ML2 from '../../assets/ML2.png';

import VRI1 from '../../assets/VRI1.jpg';
import VRI2 from '../../assets/VRI2.png';
import VRI3 from '../../assets/VRI3.png';
import VRI4 from '../../assets/VRI4.png';
import VRI5 from '../../assets/VRI5.png';
import VRI6 from '../../assets/VRI6.png';

import ARI1 from '../../assets/ARI1.jpg';
import ARI2 from '../../assets/ARI2.jpg';

const items = [
  {
    src: "https://images.unsplash.com/photo-1550052558-11de18b04282?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",
    altText: 'Slide 1',
    caption: 'Slide 1'
  },
  {
    src: "https://images.unsplash.com/photo-1554474051-0256b98c36f8?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",
    altText: 'Slide 2',
    caption: 'Slide 2'
  },
  {
    src: "https://images.unsplash.com/photo-1548131089-d5d36b219767?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",
    altText: 'Slide 3',
    caption: 'Slide 3'
  }
];

const ServicesCarousel = (props) => {
  const [activeIndex, setActiveIndex] = useState(0);
  const [animating, setAnimating] = useState(false);
  const next = () => {
    if (animating) return;
    const nextIndex = activeIndex === items.length - 1 ? 0 : activeIndex + 1;
    setActiveIndex(nextIndex);
  }

  const previous = () => {
    if (animating) return;
    const nextIndex = activeIndex === 0 ? items.length - 1 : activeIndex - 1;
    setActiveIndex(nextIndex);
  }


  const slides = items.map((item) => {
    return (
      <CarouselItem
        onExiting={() => setAnimating(true)}
        onExited={() => setAnimating(false)}
        key={item.src}
      // onMouseEnter={()=>setBg('rgba(255,255,255,0.5)')}
      // onMouseLeave={()=>setBg('')}
      >
        <div style={{ flex: 1, display: 'flex', flexDirection: 'column', }}>
          <div style={{ flex: 1, display: 'flex', }}>
            <div style={{ flex: 1, display: 'flex', }}>
              <Card style={{ width: '100%' }}>
                <CardActionArea >
                  <CardMedia
                    component="img"
                    alt="name"
                    // height="140"
                    image={VRI4}
                    title="name"
                    onClick={() => alert('ok')}
                    style={{ height: '50vh', cursor: 'pointer' }}
                  />
                </CardActionArea>
              </Card>
            </div>
            <div style={{ flex: 1, display: 'flex', }}>
              <Card style={{ width: '100%' }}>
                <CardActionArea >
                  <CardMedia
                    component="img"
                    alt="name"
                    // height="140"
                    image={ARI2}
                    title="name"
                    onClick={() => alert('ok')}
                    style={{ height: '50vh', cursor: 'pointer' }}
                  />
                </CardActionArea>
              </Card>
            </div>
            <div style={{ flex: 1, display: 'flex', }}>
              <Card style={{ width: '100%' }}>
                <CardActionArea >
                  <CardMedia
                    component="img"
                    alt="name"
                    // height="140"
                    image={CC1}
                    title="name"
                    onClick={() => alert('ok')}
                    style={{ height: '50vh', cursor: 'pointer' }}
                  />
                </CardActionArea>
              </Card>
            </div>
          </div>
          <div style={{ flex: 1, display: 'flex', }}>
            <div style={{ flex: 1, display: 'flex', }}>
              <Card style={{ width: '100%' }}>
                <CardActionArea >
                  <CardMedia
                    component="img"
                    alt="name"
                    // height="140"
                    image={Mobile2}
                    title="name"
                    onClick={() => alert('ok')}
                    style={{ height: '50vh', cursor: 'pointer' }}
                  />
                </CardActionArea>
              </Card>
            </div>
            <div style={{ flex: 1, display: 'flex', }}>
              <Card style={{ width: '100%' }}>
                <CardActionArea >
                  <CardMedia
                    component="img"
                    alt="name"
                    // height="140"
                    image={AI2}
                    title="name"
                    onClick={() => alert('ok')}
                    style={{ height: '50vh', cursor: 'pointer' }}
                  />
                </CardActionArea>
              </Card>
            </div>
            <div style={{ flex: 1, display: 'flex', }}>
              <Card style={{ width: '100%' }}>
                <CardActionArea >
                  <CardMedia
                    component="img"
                    alt="name"
                    // height="140"
                    image={ML2}
                    title="name"
                    onClick={() => alert('ok')}
                    style={{ height: '50vh', cursor: 'pointer' }}
                  />
                </CardActionArea>
              </Card>
            </div>
          </div>
        </div>
      </CarouselItem >
    );
  });

  return (
    <Carousel
      activeIndex={activeIndex}
      next={next}
      previous={previous}
    >
      {/* <CarouselIndicators items={items} activeIndex={activeIndex} onClickHandler={goToIndex} /> */}
      {slides}
      {/* <CarouselControl direction="prev" directionText="Previous" onClickHandler={previous} />
      <CarouselControl direction="next" directionText="Next" onClickHandler={next} /> */}
    </Carousel>
  );
}

export default ServicesCarousel;
