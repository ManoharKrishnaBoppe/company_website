import React from "react";
import { Row, Col} from 'reactstrap';
import { appStyles } from '../../utils/appStyles'
import ServicesCarousel from "./ServicesCarousel";
// import Nav from "../../nav/Nav";

const data = {
  EAP: 'Hitech solutions designed to help organizations achieve their strategic objectives and run business better and cost effective.',
  PD: 'Building products on supply chain and CRM domains.',
  CT: 'Helping enterprises in cloud solutions and SAAS platforms.',
  MP: 'Helps in enabling enterprises with hybrid and native mobile platforms for transactional and analytical apps.',
}


export default function Services() {

  return (
    <div style={styles.container} className="section" id="services">
      {/* <div style={{ height: 100 }}></div> */}
      <div style={{ height: '100vh', flex: 1, display: 'flex', }}>
        <div style={{ height: '100vh', width: '100%', }}>
          <ServicesCarousel />
          <div style={styles.content}>
            {/* <Nav /> */}
            <div style={{ flex: 1, display: 'flex', flexDirection: 'column', height: '100vh', width: '100%' }}>
              <div style={{ flex: 1, display: 'flex', }}>
                <div style={{ flex: 1, display: 'flex', justifyContent: 'flex-start', alignItems: 'center' }}>
                  <div
                    //onClick={onClickItem}
                    style={{ background: 'transparent', border: 'none', }}>
                    <h2 style={styles.headerText}>Virtual Reality</h2>
                    <h6 style={styles.infoText}>{data.EAP}</h6>
                  </div>
                </div>
                <div style={{ flex: 1, display: 'flex', justifyContent: 'flex-start', alignItems: 'center' }}>
                  <div
                    ////onClick={onClickItem}
                    style={{ background: 'transparent', border: 'none', }}>
                    <h2 style={styles.headerText}>Augmented Reality</h2>
                    <h6 style={styles.infoText}>{data.PD}</h6>
                  </div>
                </div>
                <div style={{ flex: 1, display: 'flex', justifyContent: 'flex-start', alignItems: 'center' }}>
                  <div
                    ////onClick={onClickItem}
                    style={{ background: 'transparent', border: 'none', }}>
                    <h2 style={styles.headerText}>Cloud Technologies</h2>
                    <h6 style={styles.infoText}>{data.CT}</h6>
                  </div>
                </div>
              </div>
              <div style={{ flex: 1, display: 'flex', }}>
                <div style={{ flex: 1, display: 'flex', justifyContent: 'flex-start', alignItems: 'center' }}>
                  <div
                    //onClick={onClickItem}
                    style={{ background: 'transparent', border: 'none', }}>
                    <h2 style={styles.headerText}>Mobility Platforms</h2>
                    <h6 style={styles.infoText}>{data.MP}</h6>
                  </div>
                </div>
                <div style={{ flex: 1, display: 'flex', justifyContent: 'flex-start', alignItems: 'center' }}>
                  <div
                    //onClick={onClickItem}
                    style={{ background: 'transparent', border: 'none', }}>
                    <h2 style={styles.headerText}>Artificial Intelligence</h2>
                    <h6 style={styles.infoText}>{data.EAP}</h6>
                  </div>
                </div>
                <div style={{ flex: 1, display: 'flex', justifyContent: 'flex-start', alignItems: 'center' }}>
                  <div
                    //onClick={onClickItem}
                    style={{ background: 'transparent', border: 'none', }}>
                    <h2 style={styles.headerText}>Machine Learning</h2>
                    <h6 style={styles.infoText}>{data.PD}</h6>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

const styles = {
  container: {
    width: '100%',
    height: '100vh',
    backgroundColor: appStyles.colors.PRIMARY_BG,

    position: 'relative',
    margin: 0,
    overflowY: 'hidden',
  },
  content: {
    position: 'absolute',
    background: 'rgba(0, 0, 0, 0.5)',
    // borderRadius: 5,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
  },
  headerText: {
    color: 'white', marginLeft: '10%', marginRight: '4%', cursor: 'pointer'
  },
  infoText: {
    color: 'darkgrey', marginLeft: '10%', marginRight: '4%', cursor: 'pointer'
  }
}
