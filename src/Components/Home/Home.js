import React from "react";
import ReactPlayer from 'react-player';

export default function Home(){
    return (
      <>
    <div className="section">
      <div className="home_content" id="home">
      <ReactPlayer playing={true} loop={true} height='110vh' width='100%' controls={false} muted 
      style={{ width: '1131px', height: '637px', left: '0%', top: '-14px',position:'relative'}} 
      url='https://www.youtube.com/watch?v=-kdUoJ7xZzM' />
      </div>
    </div>
    </>
    );
}