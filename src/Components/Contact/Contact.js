import React from 'react';
import { Button } from '@material-ui/core';
// import Nav from '../../nav/Nav'
import MailOutline from '@material-ui/icons/MailOutline';
import Phone from '@material-ui/icons/Phone';
import LocationOn from '@material-ui/icons/LocationOn';
import LinkedIn from '@material-ui/icons/LinkedIn';
import Twitter from '@material-ui/icons/Twitter';
import YouTube from '@material-ui/icons/YouTube';

import { Link } from "react-scroll";

// import './Contact.css';
import ContactUs1 from '../../assets/ContactUs1.png'


const location = 'https://www.google.com/maps/place/ModulusPI+IT+Solutions+Pvt+Ltd/@17.4562363,78.3735959,15z/data=!4m5!3m4!1s0x0:0xa256a1aa4b112d2a!8m2!3d17.4562363!4d78.3735959'
const mailto = 'mailto:someone@example.com'

class Contact extends React.Component {

  constructor() {
    super();
    this.state = {
      name: '',
      email: '',
      message: '',
    }
    this.handleChange = this.handleChange.bind(this);
    this.onClickSubmit = this.onClickSubmit.bind(this);
  }

  onClickSubmit(event) {
    const { name, email, message } = this.state;
    event.preventDefault()
    alert(name + '   ' + email + '  ' + message)
    var data = name + "-" + email + "-" + message

    // var details = $('#txtFirstName').val() + "-" + $('#txtLastName').val() + "-" + $('#txtEmail').val() + "-" + $('#txtcomments').val() + "-";
    // $.get(
    //   'http://120.138.8.110/ModPI/Home/PushEmail/?ToEmail=' + details,
    //   function (response) {
    //     alert(response);
    //   });

    alert('Thanks for contacting ModulusPI, our team will get back to you soon');


  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }


  render() {
    return (
      
      <div style={styles.container} id="contact">
        
        <img
          // src="https://images.pexels.com/photos/595804/pexels-photo-595804.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
          src={ContactUs1}
          alt='bg'
          // src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMTEhUTEhMVFRUVFRcVFRUXFRUVFRYWFRUXFhUVFRUYHSggGBolGxUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGxAQGi0lICUtLS0tLS0tLS8tLSstLS0tLSstLS0tLS0tLS0tLS0tLS0tLS0tLS8tLS0tLS0vLy0tLf/AABEIAKgBLAMBIgACEQEDEQH/xAAbAAABBQEBAAAAAAAAAAAAAAAEAQIDBQYAB//EAE0QAAIBAgMFBAUHCQUFCQEAAAECAAMRBCExBRJBUWEGE3GBIjKRodEHFEJSsbLBI2Jyc4KS0uHwJCUzs8IWNGOUohdEU1RkdIOToxX/xAAZAQADAQEBAAAAAAAAAAAAAAACAwQBAAX/xAAuEQACAgIBAwEHBAIDAAAAAAAAAQIRAyESBDFBEyJRYYGRsfAFMnHhocEjUnL/2gAMAwEAAhEDEQA/AMbQJgePfM+EkDk6QXEUyDnPYlLR4kY7Iabm8KWmTIKJAMNDHhAig5sdQogGEuMoPSQ3zktarYRypIS7bKxPWMRhnGB8yRIkckyeyimWSsAIx64kKoTJFoQ7YFJdxhqk6RRQY6yenTtJ92EoWY5pdgGrg8uJ8oNgqZucj7DNftralakKa06jKO4pGwtru6ykwnaTFkm+Ic+z4QZwipIzHkySg3S+v9ARQ20PsMbh7/VPsMtv9ocXb/Hf3fCF7D25iXrUlau5DVUBGWYLAEaTVFWa5TUW6X1/oot4xQhh+0ltWq/rH++ZX4nFqnU8h+PKG4pbkxkG5dkSrTjsHXtVKHIGxU9Rw/rlK3C4h6lQX0Gdh7h1ztNTszYpa7VkAN/RBJuB1txhY3y3ELK1CPtjKuIIECveS4ynZ2HAGwgeJq7olTkoqxeOFkO0sbYbi+cpmMezXJJ8Yzdnm5JubsujFRVIZGxzngI2SyYxI6KJ0SAaLeKGjZ06ziQPFuIxVkqpNVsF0hCIkk3YjpCoFSRGTGkzjEghhOEpBjDxhF5Stw9bdkxx5j4SglsnyRm3o0OHtA9oamTiqBpAcTUuTKZNVRFBPlYlG14X3wGkAopnDVoc4MbDnVid8TpOeix1k9JQJNUGUNRsW5V2KjDrYkRbAGLT9YziucVWhr7hSuIhcxyIIpYCNoVYxVMI37CDGuBxjHxPITlJI5xbLntOf8L/ANtS+6Zm8CMzPQsFhcdVp02GBwlUd2oRqliSgHo39PlNZsTYCmkDXwFBKue8KdOkyDM2sS9zlaZNrldnYm4wqjxM4kXtLfsqQcVhxzrU/vCexDs5hwCTgqRbO35GiB0+mZnKeA2irBk2Zs9SpupDAEEaEEHIwYtXdjXPkq4nmfafGEYmui5WrVATx9c6SjCz0D/s7xmJ73Eu1JHepUbu8zchzcA6AXvaYpqBViDqMiPDWBNOTtlOJxqomk7EbNHpViNDur48T7xNTVMTZGEFKgicQtz4nM/bBds4nu6ZY6nIS+EVCKR5M5vNmdfwij2lUBdrG+coNo1LndHDXxnUsX6ZtncG/Q8DG7kTky840j18eLgCkWzkT3PhyhrrB8Ph2dxTUXJ05AcSeQElkvA1ulZClIsbKCfCLWw7JbeUrfS4sD4GauhgAg3E/abix5n4TQbI2atRGSoAw5HMQp9PS2TR6q3SPMIk222OwxBLUHAH1Hv7m+ImRx+Cei+5UXdbXoRzB4iRSTRZF2Dx9NLxqiTATomSJFSPkO+3KIHYxvJCeLJpxEj3WnAGdZ3H5jKq2kd5NUGUggSGRejp0S868CwqNRSoc4LjEAJtDqeUBxzZmelJLieVBvkRUTnDQpgNGpnCTXMGLQU07CaaSamjOwRBdmyAuBc+JyleCxh2yKB75SfzvuNDTFyVJsenZbGXJ7g/v0v4pJS7IY5j/gf/AKUv4pQ4WkBlYeyFYFLVFt9ZftEFJMN81e19P7Fr03VmRhZlJVhyINiMpD3ZMvNuqPnNf9bU+8ZX9BnCcAVkBVoc4QmHHASRV/mfojxPE9Ifh8KWzA8WawA8B/XhDhjTAnlruBCmbWBbTRQT8JLsSnaopq94UDDfBqKjFb5gAnW0PqLQUflHZ/zV9FfK/wAJ2z9qYNb2oXtxOf4CNcaktgxm3B1Fv5Fftj06zCj3q07ncArozeBF75DrAK9GqutWsn6zfUe0EzRU9ubOb1sMB13f5GHYbDYCrlQrNSY8AxHtU6+yL4qTuxyzemqlBr5f7MQorKL777vMOSvuMm2Xhe8qonM5+AzPumm2j2eekN4AOum/Ssr2/Op+q/hkTG9k8GDVZ7A7qkbwyFyRkynNTkZyx7Q59VD05TiaDD4YWvbWY3t9VKlFBtfe91ufjPQ9yef9u2DV0p/VUMcuGf8AKMzfsZ5/6fk55jP4eiFUczmZKFmnTZ2ARUFarWDlFYgC49IA5ZdYbR7P4OrTd6NSqSqk2NhmBcZEZxKhWj1H1UVtp/QxlDDM7bq+fIDiTLmjh1oLkMz6zcTb8Oku8H2eKU7HIkXcjW/K/ITN7VZu9NJCW/DxPLrGKKxrk+5LPqFnfGL0hDtchgKa3J98PobRrLYswzOijIefGR4DZgQc2OrfgOQlvhNmBjnoIjLHI1cmdiyYlKor5k6BqijeZiTyJH2RX2Ir5OoYfnDe+2WuApCmM7C0e+0aQ0ux5KL+/SQTR62PsZ8djaDagr1UkW/CYrb+zDhqzUmNxYMrc1bQ24HIjynpJ2jU13R4DO3S8yfbjA1arDE7noLTVG4lSCxufzfSEWmHKPkyQPWOkZtG95yhqVCuNhAB8Y5TI1RjqbR3dgc4zYrQ6otx5QK0KIPlBSIE9hw0JaLadOixhoxvGC10IveWdMwHHamelKOrPKhLdEOHQXhyoIFhxnDgsyBs3sehEsNmj8ov7X3GlctoVs/EqtVS7bq5gmxNgVIvYa6x0WkImnTop6OsJwY/Kp+mn3hLGns7B5n59f8A+Fx75ZbP2Zs7eDHaGYINu6bgbxaVdxjyJ3V/SvuBbfp3xVf9c/3jI6WFA567pI1Y/UT8T/Rk2jiQ+Iquh9F6lQq+dgm8SaltdPxllRp91SFeoN1SCKa5XCjLT6zE5n4Rypk85OEQKthwgDPbLRRovQcz1ldWxROmQ98GxuMeqxY+Q4ASAU2MH1PchsMPmb2T1GyMr0qWVuuXxhT0TYyvtkB4mLySdl+GKrRyCSUxEUSRRlASKC12ftyrTsLll+qT9h5Tc9nKtKsGq08mNlbnlnZueus8zE9A7A0dygHI9d2Yi9rgHdA9iyjHKT0eZ+o44LHyWnZo2XL7en8p5l2oe+JfyHu/nPUMVco7ItyoJC31/N8OE8r2lT3rnPO7oTqVv6aHqpv5eULI7iTfpsVGTZLtn10/U0f8sS47G4NmqO9vQFN1J6sug68ZU7Sp71amtwt6NHNjZR+TGpnoeAwop0BSsMqZBHMlTvXtzJmryP6nNwxxj5Zl9o9st2m16Rv6ofK1+ZXh5QLCYYIgcm71bMx1Oedo/Z2wXxFMmqTToq1wBozDIkA6AAAX6QehgAK26GJAFxflCV3YnJHDFOON0/Jf4fCXGWkgfbiI3d0rMwyZvoqeXUyu2ntUupo0Dlo9Qe9U+PsguA2Rax0tpbTzk/UZG9RO6LBT55Pkv9s0FOiahuSW8dPZwljTwfSRYOoFXMgQbGdo1XKn+UfgAfRB/Ob4Tz5JI9zG5MsalNV1sIzaLL83rEi47p79RumZqg9Wo4aq1zqBoo8BNHhU7xGQ/SUqb9RaIbKOOjxoJzj1tGPhmDlH9ZWKtyupsfeJM1MDlDiJkKtfrFBkX9coqVAOENS94tx9wQoteBQot6JtnlBZ0zoE2HpbxhnzEQLD1d0wn/8AodIeNwrYGRZL9kuO/ECxFS94ctEQPFrYmVTuiKFWQ0GN4V6Uhw+sOBmRWjZvYOKbGK2HMJUx1QZRigL5lXRXUR9CkN63Mge2NpamSUjmT0+3L8YpDm2afYeAFV1QaOc+lKnz6Ei5/RhfbYuaqrb0AilACGAX6INuNtepMbsBxTSs/wBVBTHiR6Xts/tlB35N753JPt1lV0jz0uWS/cRbseEiX6/DzHCajsrssPepUW4U2VSMibXJPMZiFCgsk+CtmfwWBarUWmurG1+Q1J8gDN9s3s3hqKgCkrni7qGY+3TwEsFUDQAW0yGXhHgzZq2TPqZSVLSKLbnZOjWUmmi06gF1Kiyk8mAy85jMN2Txbi4pED84hfcTeepKZMjRMkU4ernFV9zxjauyq2H/AMamycidD4EZGej7Dod3QpJyRb+Nrn3zT1cJTr02pVVDKw0PDkR1lM1LcYqfo5ezjCwNNsHr80p442VeL28KGKo0zaz+t0DZL7TceEpe1+AFKq27bdf8vT/SFu8X9pSD4iZztFjO9rs40Nt3wXIfZfzm12ziRUwGHxJzKFN/qCQrjz329k7lcmNji9FQfv7/AM/n2M+e21Sju0lo0XCqoDEG+7Ybt+tpYYXt+XUq6Ij6A57p+BmNxlDdtfNlZ0J/RY29xA8oGyiI5tStlj6bFOPY2dHtEVpim4ORNrcQTcD3wPE06jE94N0MbEA5sOAJGi9OMg7JYQtvM3+Gml/ra5csvtEsq1fvXsugNr+HPrKU3OFshlBY8jUfmyTA4LSwsOkF2zt9UvTo2d9C30FP+o9IN2hxxUCkjEfXIyv+bM8lEnJRIuoyU+MS7o8Nr1JltRwDN6VSozlszcki/hpLrA0BwEg2WPQF9RCK2PSmMz+P2SfgkrL1Nt0WOGw12E0OHogeMyuycd3jBtBwF9erH8JraSZXvcmLG20eSdqqW5jK4GV33v3wG/1SoYzS/KHhSuK3rZPTU36glT7gPbMwFnIWxYoE4U4+0NIBsizU9IlQWPThJbXyjd3K3Ef0ZjRtkMSLadaLDNWGtAMY1yY8+MQUgeM9OTbVHkxSTsjw5zhoYQUUQOIjio5zI6NlTCO8HONqVhzkG4Oc7cHObyZnFA9NszJKLXJ8QPfJRTWcqKL58j7D/OBTDcky1XF/knzyJJI63I/EytGIEL3VKML8W+28A3F5xrbFQjHZs+wuDR9+sQCVYKoOe7lcnxzA8jNgTPN+ze2fmzNlvI9t5b53GhXr9uU2eytvUq7FE3gQL2YAX52sTpHwao83qsU+blWi2EURBHia2TxRwkixqiJiMVTpjeqOEHMm0VJlEIllghnMt202h3aVnU5n0F8T6N/tMTFdt6IqJQw96j1G3S9rKgtckX1MzXbfEb27TByWzsfG4H5+2Dji1citwcpwg1q7MuW3gvorobmxAFidbeI9s3Bq032Wy01KqtJgRcm7r6W90zN7TC1CSgCZDetnxJsAT8JuHFNNmEICLowYk3u7NukjkL6Cbj7v+B3WPUP/AEvuY3bAzbj+UB1+ugYmVJJlptds2/WED9gBRKsxOT9xfi/ai12Ttfu6bU2yBO8DrmQAQbZ8BLfZFdSpFO7Ees9iACc7C+p+wWmQJlhs3bHdU2pnQtvXGuYAz9kZizU0pdifqOn5JuPcL2xhrvdTcG385Lh1Sgm9U1PqjifD4yvbbKg+ipbxyHj1Psg9bHrUNy2fXLyHCJyShycl3G445OKi+xJitpPVZV9VN4eiOOf0jxk+17jdHSB4OjeonLe+zOXW0aG8PCTU2m2VKSTSQHsdqm8ArT0zYlZtwK5ufjMdsOgtsiL8mH2GaTA1bGxgKNDHK9FL8pCjvKPPdf2XX4zE1MONRlNN8oWMBxNFOK02v+2wsP8Ao98oYUabEztFcykazgYeywapShuFArIn3IxHqIwraKGmI5/AHqCxIjbybEjMHnIbRUtMdGmjQ/NusGrArfOWSmAY05mehOKSPLhJt0R0kudYR826yHDaw2ZFKjZtpkHzbrEbDdYSsfVAtD4KgObsq6d8xFoUje19cvh751LUyRNYpIc2WmAwm8bC53gCB1Hon7L+cBfA2JF9JbbPcghl1Hpr5euo68fIc5Y7fwIIFamPRYXy4cx5X9hEqWNNETzOMq95mlw3WEKO7s4YqRmCMiD0tJEA45DjK/amOpmyKSc7k8NMrHzhSUMcbH53PJKvBoMF8odRMqtIVB9ZTuNbqLEE+yW+H+ULDtrSrA9Qn27880UjnHB7EeMmWSXke+hwvdUemYrtwNKVEknQuwA8wPjMhtja1Wu+87Xt6oGQHgOEhVt0X55W6cf68YxaV8xpxHHy5xr+AWLDjx7SDuzI38XT3h6oZi2hGR14GP23W712f6Jc2A9YhQAMvDjBNn4sU6jsDmKLhRwBYqPM9ekGrg7oJNr3Nzr48+EzlUaMcLyc/hX3Y/Bg1GsB6rDcHDfIIv5XXzIm07QoKVGjQ4CzN1Siu8T5nd9sj+TvChhUqPTBRbbtR/XBU3O74m3PMys7R7Q76qzk+i+Q6UabXZh+kwy/RhQ1Ejy/8mdLxH8/PmZrGNoDrbePix3vsIgpk1d94kniZBUMmkemlSIjImMkYyIxbCRwg41hHA+EhTjJ8ngdEloYpkN1PlwmkwO1FqLybiD/AFmJlWjVYg3GRgxm4mygmegbJxCX/nLDFbapU7uzaaLfMzz6hjr5NcdRFq0ON+t/5wuVrQNb2M2lj2rVmqtqzX8BwA8pYyk1aXYE3F5AzeBbRrrHTjKSUhAkdSiDJmGcdMqwuVAbYfK3sgZloeXskb0QTmIqWO+w2GSu5Yd4YNWa95Yqt4DixmZVNOiKDVjMPUzhffCC4dRCu6E6N0bOrDtmYJq2+VZFCAFmdgijeNhmesO/2fY/94wv/wB6QfZtIfNsWOYo/wCbKh6EPshK23suqPZJ73+c4P8A5hIRS7H1Cf8AecH/AMwsyVNdY6jTMBP4DnCX/b/Brcfsl8KyJUZDvqKiOjby6kA38pY7Jxa2NOp6jGxH/hv4cj8RBe2IO5gbf+Tp/a0pcPiGHja2ejD6rfH+hXCXvIsmLnHuL2vwTUTur6jZkjlwHhMteapdsH0kqDvE4qcnp5aqeK9c+o0gFbYYqelhmDj6v0h+z8LjwiMseb5R+hZ083iio5PqUxUHPyMZc3tw58gNTJ62BqIfTVraEgEgdbiTPs00h+XBp3zCnJmXhYa2JzvytzkrUvdRcpxrvYeg3lDHJbDPyyA5mcXPD0R75CuLBVbDQZch5L8bmT4bA1ap9BGa/Gxt7TlKrvsLety0RU7FiRk2lzofLRT/AFlLjZ3ZmviKqI6siBQzuwIyJJAW+pOt+Fofs/ssqDvMU4CjMqDYftPw8vbLHa3aW6AUiQtgvefSIGVqKnX9LQQ1j17RFl6lyfHDv4+EP7RbRp0qXzajlTSwqsuv6tfzjn4C5mfTYeKxCs9Ol6JtvMSqIoHq01LEZAWv1HSA43FW6EX3VvfcvqxP0nPE/Cw9J2hV/ucL/wClX7onTfg6EPSS+P5+fjPP37GYsfQpnK4ArUiSByG9npA6PZPF1EFQUgqnQ1HSnfwDkS42PU/tWA6UiP8Aqqy++VmrfDUx/wAVfutFSikN9afJRtb+H8mDxnZPF01LGmpC2uEqI7C5AHoqSdSJw7HYzK9NVJFwrVaSt+6WvNP2Bq22hieoPuqCQ9tqt9qUjypj/XBcI9/zucs2Tlx12vt8P5MdtPY9WgAagUBiQCro4uLEi6E21EqAbGWzt/ZKY/49T/LpStqJfOSZlu0XYm69oVVU/SI8v5xfm45k+QH5we0cGMUpLyhrT8Ml7sDh7YrOd3XXK3Dr+Ej70zma4Hn+E5teDqfkSnr5iXolHSGfn8ZeiOwLTEdQ9oYdfbOVolju95cWJay2zsp3SfaIwngQQRmQRYjPjHWIolI1jIl85xM4yhtQXjqbZRs60wIslrCB4prkwnuYHXW142TdE8ErH5cZwuAUSYR3pmRegprZoNh0GqUcSiDeYilYZZ2qXOvQTn7PYkjKkfavxlfsjbJoFiEVt4AZ2NrX5g85ZP2tJBBpUyCLEFVsRy9WOUo1sjlDKpvitAlDsfjScqB/eT+KFUuw2Pv/ALuf36f8UHw3aZAcsJh/3U/gl1Q+UeoMvm9P3fwRevDKLyeUL23otT+aU3FmTCorDI2IZrjKZsNDO0O3Di6i1GRUsu7Yaak30HOVOIqWUk8vtyj4zpCuFuiuqYjeYnrcEZEcrGPouQbgm+t1IVvNTk39ZzsJgHYXAsOZyHlzktTZx4MCfOTKE37VF/KH7bNFsrtU62V+7q/rPyT+BJuPfL6t2lw9T0q2ELkDUGlUyHAHfMw2wNkYjEVhQVgMixZxvIFHEA6nMTWY75NH7tmpVaNV0BJQ02p71hewZW1jPUlWyaeDApe5/nyLNNs4VQCmDK8riktvPeEBxnask2VaVPh6xqHyCZe+S7L+TVxSTvatGm7C4RabuBxsWLZmZLtTha2DqtSdl3rAqUAUMG0OXgYfrJKwI9LilKu4m0tovVqDfJITjUta/NaAyv1NxI6mKN7gkk6sfW8vqjwlXhKW6M9TmYUDFxm3tl6xRjpCmekbRrf3SP1CD7BPNiZsNm9qqPzZaFVWUoAN4AOCB0uCD0hKhPUwk+LSumUmArWrYQ8qZ+/Wmh+VWp+Spj88fdf4QStt3CgG1RybEAdyRqDx3zbXlHYnthh64U1Q9JgBdQnei+d7EMuWfETG9MTJTc4zUXoA7DVv7wq56h/81JH2xq/3mOiqPc0K/wBqcPSO/TL1GBHolDTv6QJ9Is3AHhHYjtRhKjGoWqIT9Hut4j9oVAIDaqrM9v1OfF9qMWW/s6fran+XRgsve020KNXuxRZmC7xN0Ketu8N4/VlCZLPuejibcbaoayX0yMiPWSmKc9YlpMcnRAYqi8VqZE5VPAQaCslpUjrbK+ct0rK2hkNJgFA/q8R1U5++VwjwWiOcub2T0a5RQhUEbxsc7gP6xtzhGFO+HVcyWG9fL0QCbm/C5PKAFyON/HWd3YPtv5wlJoW4pj9yxta1hY9SMr+dol45zGXnGiqY4GMBiEzrOotqZgWMOZkgYiDVmveNlLVE8I1Kx2HMLygdAwuZHsbPud3YjKlGS0xnJK1rQ+NoDk0ypXjOpEx9PUxyaxNFDZKtUyalZyu96oa7eAvl7bTqKgkX04xmKq52GQ5Q3Lidjx89hWKx28SBpyiI2UDpYZmBYaaZ8TJsOrDI2joTk9sz04LSNZ2Bb+0N+rP3lnooq2U+B+yebdhDbEn9U33lnoDvZSeh+ybON0ef1EuORhOx9qNVpK7UalMkeoSt/EWbQ+U8b+UfaLVscd+k1Lu1ChXtvEAk7xtlneexU7GxyNsx08J5d8p2IVcap1PcrvXsbi7WERKK34KelyNzMojyQNBEbpYcuXISYNOTPRJiYxmjC8jLTWzRXaQOY9jImMW2YxsSKY2JbNSOiGLEi2GNnARYomUbYqx5UzgJIhjEhcmRB44VDFcSErBbaNVMl3rx6PbjBo9DOUjnEJ7znHXkF4l+UPkBwCC04LeRK95JvQk7AcaLdFvAsUuZiTpTNeyR437QyksmsROnQIrQyT2OWrHNVE6dNszigSkdY5RnOnQEMfcJAigdIk6NQpMP2O49NDwO8PBv5x+IwjXy90SdKsS5Rpi8knGVoGWs9NgwJVgciMiPOG1du4iou61Viuh0F/GwznTop2pNFFRlFSaVhmD25iFVUpVmtpnY2A6kEzO7VwNd6jVGPeE5klrn3xZ0a8Uckdk3N4pPiioL2NjqI9ak6dPMUndHp1qx2/Gl506FZg0mNJnToLZqGmJOnRbDEnTp0E06OURZ01GD7RLxJ00wcTeRzp0xnISLedOmGib07enTplnDqYJ0jzhm5RJ0dDGpK2JnkcXSP//Z"
          style={{ height: '100vh', width: '100%' }}
        />
        <div style={styles.content}>
        {/* <Nav /> */}
          {/* <Nav /> */}
          <div style={styles.body}>
            <div style={{ flex: 2, display: 'flex', }}>
              <div style={{ flex: 1, display: 'flex', flexDirection: 'column', }}>
                <div style={{ flex: 1, display: 'flex', width: '100%', justifyContent: 'center', alignItems: 'center' }}>
                  {/* <h2 style={{ color: 'white' }}>GET IN TOUCH</h2> */}
                </div>
                <div style={{ flex: 2, display: 'flex', flexDirection: 'column', justifyContent: 'space-around', alignItems: 'center' }}>
                  <a style={{ flex: 1, color: 'white', textDecoration: 'none' }} href={location}>
                    <LocationOn style={{ flex: 1, fontSize: 40, color: 'darkgrey', }} />
                  </a>
                  <div style={{ flex: 2, display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                    <h6 style={{ color: 'white', }}>
                      MODULUSPI IT SOLUTIONS PVT LTD.</h6>
                    <h7 style={{ color: 'white', }}>
                      LEVEL #2, ALEKHYA HOMES PRANAVA
                    </h7>
                    <h8 style={{ color: 'white', alignItems: 'center' }}>
                      JUBILEE ENCLAVE, HITEC CITY,
                    </h8>
                    <h8 style={{ color: 'white', alignItems: 'center' }}>
                      HYDERABAD 500081, INDIA
                    </h8>
                  </div>
                </div>
              </div >
              <div style={{ flex: 1, display: 'flex', flexDirection: 'column', }}>
                <div style={{ flex: 1, display: 'flex', width: '100%', justifyContent: 'center', alignItems: 'center' }}>
                  <h2 style={{ color: 'white' }}>GET IN TOUCH</h2>
                </div>
                <div style={{ flex: 2, display: 'flex', flexDirection: 'column', justifyContent: 'space-around', alignItems: 'center' }}>
                  <a style={{ flex: 1, color: 'white', textDecoration: 'none' }} href={location}>
                    <MailOutline style={{ flex: 1, fontSize: 40, color: 'darkgrey' }} />
                  </a>
                  <div style={{ flex: 2, }}>
                    {/* <h5 style={{ color: 'white', cursor: 'pointer' }}
                     onClick={() => alert('ok')}>sales@moduluspi.com</h5> */}
                    <h5 ><a style={{ color: 'white', textDecoration: 'none' }} href={mailto}>sales@moduluspi.com</a></h5>
                  </div>
                </div>
              </div >
              <div style={{ flex: 1, display: 'flex', }}>
                <div style={{ flex: 1, display: 'flex', flexDirection: 'column', }}>
                  <div style={{ flex: 1, display: 'flex', width: '100%', justifyContent: 'center', alignItems: 'center' }}>
                    {/* <h2 style={{ color: 'white' }}>GET IN TOUCH</h2> */}
                  </div>
                  <div style={{ flex: 2, display: 'flex', flexDirection: 'column', justifyContent: 'space-around', alignItems: 'center' }}>
                    <a style={{ flex: 1, color: 'white', textDecoration: 'none' }} >
                      <Phone style={{ flex: 1, fontSize: 40, color: 'darkgrey', marginLeft: 25 }} />
                    </a>

                    <div style={{ flex: 2, display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                      <h6 style={{ color: 'white', }}>
                        Phone: (Toll free) (888)585-7083
                        </h6>
                      <h6 style={{ color: 'white', }}>
                        USA :(828)348-4354
                        </h6>
                      <h6 style={{ color: 'white', }}>
                        INDIA +91 9353044044
                        </h6>
                    </div>
                    <h5 style={{ color: 'white' }}>
                    </h5>
                  </div>
                </div >
              </div >
            </div >
            <div style={{ flex: 2.5, display: 'flex', }}>
              <div style={{ flex: 1, display: 'flex', }}>
              </div >
              <div style={{ flex: 1, display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                {/* <form>
                  <input type="text" name="name" style={{ border: 'none', borderWidth: 1 }} placeholder=" Full Name" />
                  <input type="text" name="email" style={{ border: 'none', borderBottomWidth: 1, }} placeholder="  Email" />
                </form> */}
                <div style={{ flex: 1, display: 'flex', width: '100%', justifyContent: 'center', alignItems: 'center' }}>
                  <h4 style={{ color: 'white' }}>WE WOULD LOVE TO HEAR FROM YOU  </h4>
                </div >
                <div style={{ flex: 1, display: 'flex', width: '100%', justifyContent: 'center', alignItems: 'center' }}>
                  <input type="text" name="name"
                    style={styles.textInput}
                    placeholder="Full Name"
                    value={this.state.name}
                    onChange={this.handleChange}
                  />
                </div >
                <div style={{ flex: 1, display: 'flex', width: '100%', justifyContent: 'center', alignItems: 'center' }}>
                  <input type="email" name="email"
                    style={styles.textInput}
                    placeholder="Email"
                    value={this.state.email}
                    onChange={this.handleChange}
                  />
                </div >

                <div style={{ flex: 1, display: 'flex', width: '100%', justifyContent: 'center', alignItems: 'center' }}>
                  <textarea type="messge" name="messge"
                    style={styles.msgTextInput}
                    rows="3" placeholder="Message"
                    value={this.state.messge}
                    onChange={this.handleChange}
                  // multiple={true}
                  />
                </div >
                <div style={{ flex: 1, display: 'flex', width: '100%', justifyContent: 'center', alignItems: 'center', }}>
                  <Button variant="contained" onClick={this.onClickSubmit} style={{ outline: 'none', backgroundColor: 'rgba(255, 255, 255, 0.9)', }}>SUBMIT</Button>
                </div >
              </div >
              <div style={{ flex: 1, display: 'flex', }}>
              </div >
            </div >
            <div style={{ flex: 1, display: 'flex', }}>
              <div style={{ flex: 1, display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                <h7 style={{ color: 'darkgrey' }}>{'Terms & Conditions'}</h7>
              </div >
              <div style={{ flex: 1, display: 'flex', justifyContent: 'center', alignItems: 'center', }}>
                {/* <Link to='/' style={{ textDecoration: 'none', color: 'white' }} >
                  <img src={require('../../assets/ModulusPI.png')} alt='logo' />
                </Link> */}

                <a style={{ color: 'white', textDecoration: 'none' }}
                  href="https://zw.linkedin.com/company/moduluspi">
                  <LinkedIn style={{ fontSize: 40, color: 'darkgrey', marginLeft: 25 }} />
                </a>
                <a style={{ color: 'white', textDecoration: 'none' }}
                  href="https://twitter.com/ModulusPi">
                  <Twitter style={{ fontSize: 40, color: 'darkgrey', marginLeft: 25 }} />
                </a> <a style={{ color: 'white', textDecoration: 'none' }}
                  href="https://www.youtube.com/channel/UCFKpPMbRmM7OgqiZC2p_a9g/featured">
                  <YouTube style={{ fontSize: 40, color: 'darkgrey', marginLeft: 25 }} />
                </a>

              </div >
              <div style={{ flex: 1, display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                <h7 style={{ color: 'darkgrey', }}>Privacy policy</h7>
              </div >
            </div >
          </div >
        </div >
      </div >
    )
  }
}

export default Contact;

const styles = {
  container: {
    position: 'relative',
    margin: 0,
    overflowY: 'hidden',
  },

  content: {
    position: 'absolute',
    background: 'rgba(0, 0, 0, 0.7)',
    // borderRadius: 5,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
  },

  body: {
    display: 'flex',
    flex: 1,
    height: '100vh',
    flexDirection: 'column',
    // padding: 20
  },
  textInput: {
    outline: 'none', border: 'none', borderWidth: 1, width: '50%', height: '50%',
    borderColor: 'red', backgroundColor: 'rgba(255, 255, 255, 0.9)', padding: 10
  },
  msgTextInput: {
    outline: 'none', border: 'none', borderWidth: 1, width: '50%', height: '85%',
    borderColor: 'red', backgroundColor: 'rgba(255, 255, 255, 0.9)', padding: 10
  }

}